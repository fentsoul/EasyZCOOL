// yjj&ly
$(function () {
    //相当于window.onload

    for (let i = 0; i <contentJson.length; i++) {
        let mid = $('.mid .content');
        mid.append(
            $('<div/>').addClass(' mid_box').append(
                $('<div/>').addClass('mid_box_content').append(
                    $('<div/>').addClass('mid_box_top').append(
                        $('<img/>').attr('src','img/content/' + contentJson[i].img)
                    )
                ).append(//中
                    $('<div/>').addClass('mid_box_mid').append(
                        $('<p/>').addClass('mid_box_mid_tittle').append(contentJson[i].title)
                    ).append(
                        $('<p/>').addClass('mid_box_mid_type').append(contentJson[i].subtitle)
                    ).append(
                        $('<p/>').addClass('mid_box_mid_foot').append(
                            $('<i>').addClass('fa fa-eye').attr('aria-hidden','true'),
                            $('<span/>').addClass('view').append(contentJson[i].view)
                        ).append(
                            $('<i>').addClass('fa fa-commenting-o').attr('aria-hidden','true'),
                            $('<span/>').addClass('remark').append(contentJson[i].remark)
                        ).append(
                            $('<i>').addClass('fa fa-thumbs-o-up').attr('aria-hidden','true'),
                            $('<span/>').addClass('praise').append(contentJson[i].praise)
                        )
                    )
                ).append(//下
                    $('<div/>').addClass('mid_box_foot').append(
                        $('<span/>').append(
                            $('<a/>').attr('href','#').append(
                                $('<img/>').attr('src','img/content/' + contentJson[i].head),
                                contentJson[i].name
                            )
                        ).append(
                            $('<span/>').addClass('mid_box_foot_left').append(
                                $('<div/>').addClass('author-info').append(
                                    $('<div/>').addClass('author-content')
                                )
                            )
                        ).append(
                            $('<span/>').addClass('mid_box_foot_right').append(contentJson[i].time)
                        )
                    )
                )
            )
        )
    }
});
contentJson=[
    {
        "img": "01d19f5e096ae5a80120a895470351.jpg@260w_195h_1c_1e_1o_100sh.jpg",
        "title": "Font design 字体设计",
        "subtitle": "平面-字体/字形",
        "view": 487,
        "remark": 5,
        "praise": 31,
        "head": "0315a3659c8deaea8012053f859e5b1.jpg@80w_80h_1c_1e_1o_100sh.jpg",
        "name": "墨希先生",
        "time": "2小时前"
    },
    {
        "img": "01e22e5e096a74a80120a895ea1c88.jpg@260w_195h_1c_1e_1o_100sh.jpg",
        "title": "《王者荣耀》貂蝉FMVP皮肤",
        "subtitle": "插画-商业插画",
        "view": 2464,
        "remark": 30,
        "praise": 199,
        "head": "04164258fed9a7a8012145508ff38c.jpg@80w_80h_1c_1e_1o_100sh.jpg",
        "name": "叁乔君",
        "time": "2小时前"
    },
    {
        "img": "014b315e095a29a8012165186a87d9.jpg@260w_195h_1c_1e_1o_100sh.jpg",
        "title": "做女人没有鼻梁也挺好",
        "subtitle": "动漫-中/长篇漫画",
        "view": 11000,
        "remark": 49,
        "praise": 250,
        "head": "04dc3655406c6c0000017c50e1b9cc.jpg@80w_80h_1c_1e_1o_100sh.jpg",
        "name": "穹穹兮",
        "time": "3小时前"
    },
    {
        "img": "0188c85e085d87a80120a895150312.jpg@260w_195h_1c_1e_1o_100sh.jpg",
        "title": "西文书法",
        "subtitle": "平面-字体/字形",
        "view": 1847,
        "remark": 25,
        "praise": 61,
        "head": "0152065d565750a8012187f491b7ec.jpg@80w_80h_1c_1e_1o_100sh.jpg",
        "name": "汝词工作室",
        "time": "16小时前"
    },
    {
        "img": "018a335e08e21ea80120a895fcf112.jpg@260w_195h_1c_1e_1o_100sh.jpg",
        "title": "坏孩子的天空",
        "subtitle": "动漫-中/长篇漫画",
        "view": 6968,
        "remark": 180,
        "praise": 782,
        "head": "017b975b5705cda801215c8f5e8b5e.jpeg@80w_80h_1c_1e_1o_100sh.jpg",
        "name": "郭九二",
        "time": "5小时前"
    },
    {
        "img": "014ae65e08a11da80120a895f9bbb7.jpg@260w_195h_1c_1e_1o_100sh.jpg",
        "title": "Lettering年终总结2019",
        "subtitle": "平面-字体/字形",
        "view": 2784,
        "remark": 3,
        "praise": 49,
        "head": "01851d5c263d57a8012029ac4d4aec.jpg@80w_80h_1c_1e_1o_100sh.jpg",
        "name": "TEONA_nuoR",
        "time": "17小时前"
    },
    {
        "img": "011e825e08a93ba80120a895fe774d.jpg@260w_195h_1c_1e_1o_100sh.jpg",
        "title": "2018-2019单屏页面设计精选合集",
        "subtitle": "网页-企业官网",
        "view": 3123,
        "remark": 31,
        "praise": 239,
        "head": "0075495953032aa8012193a3359707.jpg@80w_80h_1c_1e_1o_100sh.jpg",
        "name": "梁定然_海南",
        "time": "16小时前"
    },
    {
        "img": "031da755e08101fa801216518623199.jpg@260w_195h_1c_1e_1o_100sh.jpg",
        "title": "《水浒.天罡地煞108》",
        "subtitle": "手工艺-手办/原型",
        "view": 3345,
        "remark": 14,
        "praise": 71,
        "head": "0432165540a32f0000017c50d22638.jpg@80w_80h_1c_1e_1o_100sh.jpg",
        "name": "张墨一",
        "time": "1小时前"
    },
    {
        "img": "03146aa5e06f02fa80120a8952ce794.jpg@260w_195h_1c_1e_1o_100sh.jpg",
        "title": "山村松园同人《美人图》",
        "subtitle": "插画-商业插画",
        "view": 4220,
        "remark": 11,
        "praise": 323,
        "head": "031acc05c61174ca801213f269bec46.jpg@80w_80h_1c_1e_1o_100sh.jpg",
        "name": "绘画小能手渣熊",
        "time": "1天前"
    },
    {
        "img": "01f98f5e0610ada8012165187e293f.jpg@260w_195h_1c_1e_1o_100sh.jpg",
        "title": "Heidi",
        "subtitle": "插画-商业插画",
        "view": 4635,
        "remark": 25,
        "praise": 485,
        "head": "0315f2c5d5aa033a80120695cffaefb.jpg@80w_80h_1c_1e_1o_100sh.jpg",
        "name": "XISION",
        "time": "2天前"
    },
];
