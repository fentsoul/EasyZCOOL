$(document).ready(function () {
	fixTop();
	backTop();
	tabChange();
});


//自定义函数，实现固定顶部导航及隐藏部分内容的功能
function fixTop() {
	//当鼠标滚动到某一值的时候
	$(window).scroll(function () {
		var top = $(this).scrollTop();

		if (top > 100) {
			$(".top").addClass("fixed");
			$(".banner").css("margin-top", "80px");
			$(".top_left").hide();//隐藏
			$(".top_right").hide();
			$(".top_mid").css("float", "right");
		} else {
			$(".top").removeClass("fixed");
			$(".banner").css("margin-top", "0px");
			$(".top_left").show();//显示
			$(".top_right").show();
			$(".top_mid").css("float", "left");
		}
	});
}

//自定义函数，实现回到顶部的功能
function backTop() {
	//1.初始状态，隐藏部分内容
	$(".back_top").hide();
	//2.当鼠标滚到某一值的时候，显示此部分内容
	$(window).scroll(function () {
		var top = $(this).scrollTop();
		if (top > 500) {
			$(".back_top").show(1000,colorchange);
			function colorchange(){
				$(".back_top p").css("color", "#000");
			}
		} else {
			$(".back_top").hide();
		}
	})
	//3.实现单击回到顶部
	$(".back_top").click(function (e) {
		$("html,body").animate({scrollTop:0},300);

	});
}

//自定义函数，实现选项卡的功能
function tabChange(){
	let i=1;
	$(".shop_header span").eq(i).addClass("active");
	$(".shop_body").eq(i).siblings().hide();
	$(".shop_header span").click(function (e) {
		//得到单击span在整个span中的序号
		let j = $(".shop_header span").index(this);
		$(".shop_header span").eq(j).addClass("active").siblings().removeClass("active");
		$(".shop_body").eq(j).show().siblings().hide();
	});
}